Lazarus Build VM for Windows Applications
-----------------------------------------
 
- [Installation](#installation)
- [Compiling your Application](#compiling_your_application)

With this Docker image you can build Lazarus Applications without installing Lazarus or Freepascal

# Installation

Automated builds of the image are available on [Dockerhub](https://hub.docker.com/r/krak/lazarus-build-windows/)

# Compiling your Application
```bash
docker run -it --rm -v ~/myApp:/root/app krak/lazarus-build-windows lazbuild /root/app/myApp.lpi
```     