FROM debian:latest
MAINTAINER Kirill Krasnov

# Set correct environment variables
ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL C.UTF-8
ENV LANG ru_RU.UTF-8
ENV LANGUAGE ru_RU.UTF-8

RUN dpkg --add-architecture i386 && \
apt-get update &&  apt-get -y install xvfb \
x11vnc \
xdotool \
wget \
supervisor \
x11-apps \
wine64 \
wine32 \
net-tools && rm -rf /var/lib/apt/lists/*

ENV DISPLAY :0.0

WORKDIR /root/
ADD lazarus /root/.wine/
ADD fb /root/fb
COPY startup.sh /usr/local/bin/startup.sh

# Expose Port
EXPOSE 8080

ENV WINEPATH "C:/lazarus;c:/lazarus/mingw/i386-win32/bin;c:/lazarus/mingw/x86_64-win64/bin;c:/lazarus/fpc/3.0.4/bin/x86_64-win64"

ENTRYPOINT ["bash","/usr/local/bin/startup.sh"]
CMD ["dir"]
